import numpy as np
import cv2
from CameraModel import OpenCVModel

if __name__ == "__main__":
    intrinsic = np.array(
        [
            [2.10816732e03, 0.00000000e00, 1.89146409e03],
            [0.00000000e00, 2.12004669e03, 1.09592446e03],
            [0.00000000e00, 0.00000000e00, 1.00000000e00],
        ]
    )

    distortion = np.array(
        [
            [
                0.26262104,
                -0.01798232,
                0.0,
                0.0,
                0.00098943,
                0.65308172,
                0.0,
                0.0,
                0.0,
                0.0,
                0.0,
                0.0,
                0.0,
                0.0,
            ]
        ]
    )

    print(intrinsic.shape)
    print(distortion.shape)

    np.random.seed(1000)
    obj_pts = np.random.random((10, 1, 3)) + [0, 0, 1]
    print(obj_pts)
    rvec, tvec = np.zeros((3,)), np.zeros((3,))

    img_pts0, _ = cv2.projectPoints(obj_pts, rvec, tvec, intrinsic, distortion)
    img_pts1 = OpenCVModel.project(obj_pts, intrinsic, distortion)
    for i, j in zip(img_pts0, img_pts1):
        print(i, j)
