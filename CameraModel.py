import numpy as np


class OpenCVModel:
    def project(obj_pts, intrinsic, distortion):
        if not intrinsic.shape or intrinsic.shape != (3, 3):
            print("intrinsic shape should be (3, 3)")
            return
        fx, fy, cx, cy = (
            intrinsic[0, 0],
            intrinsic[1, 1],
            intrinsic[0, 2],
            intrinsic[1, 2],
        )
        k1 = distortion[0, 0]
        k2 = distortion[0, 1]
        k3 = distortion[0, 4]
        k4 = distortion[0, 5]
        obj_pts_m = np.squeeze(obj_pts, axis=1).T
        obj_pts_m /= obj_pts_m[2]
        mx = obj_pts_m[0]
        my = obj_pts_m[1]
        r2 = mx * mx + my * my
        r4 = r2 * r2
        r6 = r4 * r2
        cdist = 1 + k1 * r2 + k2 * r4 + k3 * r6
        icdist2 = 1.0 / (1 + k4 * r2)
        xd0 = mx * cdist * icdist2
        yd0 = my * cdist * icdist2
        img_pts = np.vstack((xd0 * fx + cx, yd0 * fy + cy)).T
        img_pts = np.expand_dims(img_pts, 1)
        return img_pts

    def unproject():
        pass
